﻿using Microsoft.EntityFrameworkCore;
using ApiBaseOrders.Models;
using System.Runtime.InteropServices;

public class ApplicationContext : DbContext
{
    public DbSet<Order> Orders => Set<Order>();
    public DbSet<OrderItem> OrderItems => Set<OrderItem>();
    public DbSet<Provider> Providers => Set<Provider>();

    public ApplicationContext()
    {
        //Database.EnsureDeleted(); // для обнуления базы до Orders.json 
        Database.EnsureCreated();
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlite("Data Source=orders.db");
    }
}
