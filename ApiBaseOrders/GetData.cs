﻿using ApiBaseOrders.Models;
using System.Text.Json;

namespace ApiBaseOrder
{
    public class GetData
    {
        /// <summary>
        /// При первом запуске получить фейковую базу данных из json. Формируем три таблицы
        /// </summary>
        public static void GetBase()
        {
            using ApplicationContext db = new();

            if (!db.Orders.Any())
            {
                string text = File.ReadAllText("Orders.json");
                var projectOrder = JsonSerializer.Deserialize<ProjectOrder>(text);

                db.Orders.AddRange(entities: projectOrder.Orders);
                db.OrderItems.AddRange(entities: projectOrder.OrderItems);
                db.Providers.AddRange(entities: projectOrder.Providers);

                db.SaveChanges();
            }
        }


        /// <summary>
        /// Фильтрация
        /// </summary>
        public static OrdersSearchModel GetOrdersSearchModel(OrdersSearchModel paramFind)
        {
            var orders = new OrdersSearchModel();
            orders.ListOrder = GetOrders();
            orders.Providers = GetProviders();
            orders.SlectedForFilterListNumber = orders.ListOrder.Select(x => x.NameNumber).ToList() ?? new List<string>();

            if (paramFind.SlectedForFilterListNumber != null)
            {
                orders.ListOrder = orders.ListOrder.Where(x =>  paramFind.SlectedForFilterListNumber.Contains(x.NameNumber)).ToList();
            }
            if (paramFind.SlectedForFilterProviderIds != null)
            {
                orders.ListOrder = orders.ListOrder.Where(x => paramFind.SlectedForFilterProviderIds.Contains(x.ProviderId)).ToList();
            }
            if (paramFind.FilterOn)
            {
                orders.ListOrder = orders.ListOrder.Where(x => x.Date <= paramFind.EndDate && paramFind.BeginDate <= x.Date).ToList();
            }            

            return orders;
        }

        /// <summary>
        /// Получаем список поставщиков
        /// </summary>
        public static List<Provider> GetProviders()
        {
            var providerItems = new List<Provider>();
            using (ApplicationContext db = new())
            {
                providerItems = db.Providers.ToList();
            }
            return providerItems;
        }

        /// <summary>
        /// Объединяем таблицы для главной страницы
        /// </summary>
        public static List<Orders> GetOrders()
        {
            var orders = new List<Orders>();
            using (ApplicationContext db = new())
            {
                orders = (List<Orders>)(from order in db.Orders
                                        join provider in db.Providers on order.ProviderId equals provider.Id
                                        select new Orders
                                        {
                                            Id = order.Id,
                                            NameNumber = order.Number,
                                            Date = order.Date,
                                            ProviderId = order.ProviderId,
                                            ProviderName = provider.Name,
                                            Providers = db.Providers.ToList()
                                        }).ToList();
            }
            return orders;
        }

        /// <summary>
        /// Получаем список позиций для одного заказа
        /// </summary>
        public static List<OrderItem> GetOrderItems(int id)
        {
            var orderItems = new List<OrderItem>();
            using (ApplicationContext db = new())
            {
                orderItems = db.OrderItems.Where(x => x.OrderId == id).ToList();
            }
            return orderItems;
        }

        /// <summary>
        /// Получаем заказ с его списком позиций
        /// </summary>
        public static ItemsForOrder GetOrder(int id)
        {
            var order = new ItemsForOrder();
            using (ApplicationContext db = new())
            {
                order.Ord = db.Orders.FirstOrDefault(o => o.Id == id);
                order.ProviderName = db.Providers.FirstOrDefault(p => p.Id == order.Ord.ProviderId).Name;
                order.OrderItems = db.OrderItems.Where(x => x.OrderId == id).ToList();
                order.Id = id;
            }
            return order;
        }

        ///// <summary>
        ///// 
        ///// </summary>
        //public static ItemsForOrder GetOrderForEdit(int id)
        //{
        //    var order = new ItemsForOrder();
        //    using (ApplicationContext db = new())
        //    {
        //        order.Ord = db.Orders.FirstOrDefault(o => o.Id == id);
        //        order.ProviderName = db.Providers.FirstOrDefault(p => p.Id == order.Ord.ProviderId).Name;
        //        order.OrderItems = db.OrderItems.Where(x => x.OrderId == id).ToList();
        //        order.Id = id;
        //    }
        //    return order;
        //}

        //public static List<Orders> GetOrders()
        //{
        //    var orders = new List<Orders>();
        //    using (ApplicationContext db = new())
        //    {
        //        orders = (List<Orders>)(from order in db.Orders
        //                                join orderItem in db.OrderItems on order.Id equals orderItem.OrderId
        //                                join provider in db.Providers on order.ProviderId equals provider.Id
        //                                select new Orders
        //                                {
        //                                    Id = order.Id,
        //                                    IdOrderItem = orderItem.Id,
        //                                    NameNumber = order.Number,
        //                                    Date = order.Date,                                            
        //                                    Name = orderItem.Name,
        //                                    Quantity = orderItem.Quantity,
        //                                    Unit = orderItem.Unit,
        //                                    ProviderId = order.ProviderId,
        //                                    ProviderName = provider.Name,
        //                                    Providers = db.Providers.ToList()
        //                                }).ToList();
        //    }
        //    return orders;
        //}
    }
}