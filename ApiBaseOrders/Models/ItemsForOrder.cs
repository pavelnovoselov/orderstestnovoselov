﻿namespace ApiBaseOrders.Models
{
    public class ItemsForOrder
    {
        public int Id { get; set; }
        public Order? Ord { get; set; }
        public List<OrderItem>? OrderItems { get; set; }
        public string ProviderName { get; set; }
    }
}
