﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiBaseOrders.Models
{
    public class Order
    {
        public int Id { get; set; }
        public string? Number { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime Date { get; set; }
        public int ProviderId { get; set; }
    }
}
