﻿namespace ApiBaseOrders.Models
{
    public class Orders
    {
        public int Id { get; set; }
        //public int IdOrderItem { get; set; }
        public string? NameNumber { get; set; }
        public DateTime Date { get; set; }
        public int ProviderId { get; set; }
        //public string? Name { get; set; }
        //public decimal? Quantity { get; set; }        
        //public string? Unit { get; set; }
        public string? ProviderName { get; set; }
        public List<Provider>? Providers { get; set; }
        public List<Order>? OrderItems { get; set; }
    }
}
