﻿namespace ApiBaseOrders.Models
{
    public class OrdersSearchModel
    {
        public string NameNumber { get; set; }
        public List<string>? SlectedForFilterListNumber { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public int ProviderId { get; set; }
        public List<int>? SlectedForFilterProviderIds { get; set; }
        public string? ProviderName { get; set; }
        public List<Provider>? Providers { get; set; }
        public List<Orders>? ListOrder { get; set; }
        public bool FilterOn { get; set; }
    }
}
