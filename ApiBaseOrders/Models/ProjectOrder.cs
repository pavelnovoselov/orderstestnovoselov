﻿namespace ApiBaseOrders.Models
{
    public class ProjectOrder
    {
        public List<Order>? Orders { get; set; }
        public List<OrderItem>? OrderItems { get; set; }
        public List<Provider>? Providers { get; set; }
    }
}
