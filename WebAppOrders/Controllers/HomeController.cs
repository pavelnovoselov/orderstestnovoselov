﻿using ApiBaseOrder;
using ApiBaseOrders.Models;
using Microsoft.AspNetCore.Mvc;

namespace WebAppOrders.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            try
            {
                GetData.GetBase();
                var allOrders = GetData.GetOrdersSearchModel(new OrdersSearchModel());

                return View(allOrders);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }            
        }

        [HttpPost]
        public IActionResult Index(OrdersSearchModel paramFind) // получить список заказов после фильтрации
        {
            try
            {
                paramFind.FilterOn = true;
                var orders = GetData.GetOrdersSearchModel(paramFind);

                return View(orders);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }            
        }

        public IActionResult ShowFormAddorEditOrder(Orders data)
        {
            try
            {
                var orders = GetData.GetOrders();
                if (data.Id < 1)
                {
                    return PartialView("_FormAddorEditOrder", new Orders() { Id = -1, Providers = orders[0].Providers });
                }

                var order = orders.FirstOrDefault(x => x.Id == data.Id);

                return PartialView("_FormAddorEditOrder", order);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }           

        }

        [HttpPost]
        public async Task<ActionResult<string>> CheckOnChangeOrder(Orders data)
        {
            try
            {
                var orderItems = GetData.GetOrderItems(data.Id);
                var orders = GetData.GetOrders();            
            
            if (data.Id < 1) // проверка при добавлении нового заказа
            {
                foreach (var item in orders)
                {
                    if (data.NameNumber == item.NameNumber && data.ProviderId == item.ProviderId)
                    {
                        return "Order.Namber в связке с Order.ProviderId должен быть уникален";
                    }
                }
            }
            else // проверка при редактировании заказа
            {
                foreach (var item in orders)
                {
                    if (data.NameNumber == item.NameNumber && data.ProviderId == item.ProviderId && item.Id != data.Id)
                    {
                        return "Order.Namber в связке с Order.ProviderId должен быть уникален";
                    }
                }
                if (orderItems.Any(x => x.Name == data.NameNumber))
                {
                    return "OrderItem.Name не может быть равен Order.Namber - " + data.NameNumber;
                }
            }

            return "";
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPost]
        public async Task<ActionResult<string>> CheckOnChangeOrderItem(OrderItem data)
        {
            try
            {
                var orders = GetData.GetOrders();
                var orderName = orders.FirstOrDefault(x => x.Id == data.OrderId)?.NameNumber;
                if (data.Name == orderName)
                {
                    return "OrderItem.Name не может быть равен Order.Namber - " + data.Name;
                }

                return "";
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
            
        }

        [HttpPost]
        public async Task<ActionResult<Orders>> SaveAddorEditOrder(Orders data)
        {
            try
            {
                using ApplicationContext db = new();
                if (data.Id < 1)
                {
                    db.Orders.Add(new Order() { Number = data.NameNumber, Date = data.Date, ProviderId = data.ProviderId });
                }
                else
                {
                    db.Orders.Update(new Order() { Id = data.Id, Number = data.NameNumber, Date = data.Date, ProviderId = data.ProviderId });
                }

                await db.SaveChangesAsync();

                var orders = GetData.GetOrders();
                var order = new Orders();
                if (data.Id < 1)
                {
                    order = orders.Last();
                }
                else
                {
                    order = orders.FirstOrDefault(x => x.Id == data.Id);
                }

                return order;
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }           
        }

        public async Task<IActionResult> FormOrder(int id)
        {
            try
            {
                var order = GetData.GetOrder(id);
                return View("FormOrder", order);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }            
        }

        public IActionResult FormDeleteOrder(int id)
        {
            return PartialView("_FormDelete", new Order { Id = id});
        }

        [HttpPost]
        public async Task<IActionResult> DeleteOrder(int id)
        {
            try
            {
                using ApplicationContext db = new();
                var order = db.Orders.FirstOrDefault(o => o.Id == id);
                if (order != null)
                {
                    db.Orders.Remove(order);
                    await db.SaveChangesAsync();
                }

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }            
        }

        public IActionResult ShowFormAddorEditOrderItem(OrderItem data)
        {
            try
            {
                if (data.Id < 1)
                {
                    return PartialView("_FormAddorEditOrderItem", new OrderItem() { Id = -1, OrderId = data.OrderId });
                }
                using ApplicationContext db = new();
                var orderItems = db.OrderItems.ToList();
                var orderItem = orderItems.FirstOrDefault(x => x.Id == data.Id);
                return PartialView("_FormAddorEditOrderItem", orderItem);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }            
        }

        [HttpPost]
        public async Task<ActionResult<OrderItem>> SaveAddOrderItem(OrderItem data)
        {
            try
            {
                using ApplicationContext db = new();
                if (data.Id < 1)
                {
                    db.OrderItems.Add(new OrderItem() { Name = data.Name, Quantity = data.Quantity, Unit = data.Unit, OrderId = data.OrderId });
                }
                else
                {
                    db.OrderItems.Update(data);
                }

                await db.SaveChangesAsync();

                var orderItems = db.OrderItems.ToList();
                var orderItem = new OrderItem();
                if (data.Id < 1)
                {
                    orderItem = orderItems.Last();
                }
                else
                {
                    orderItem = orderItems.FirstOrDefault(x => x.Id == data.Id);
                }

                return orderItem;
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }            
        }

        public IActionResult FormDeleteOrderItem(int id)
        {
            return PartialView("_FormDeleteItem", new OrderItem { Id = id });
        }

        [HttpPost]
        public async Task<IActionResult> DeleteOrderItem(int id)
        {
            try
            {
                using ApplicationContext db = new();
                var orderItem = db.OrderItems.FirstOrDefault(o => o.Id == id);
                if (orderItem != null)
                {
                    db.OrderItems.Remove(orderItem);
                    await db.SaveChangesAsync();
                }

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
    }
}