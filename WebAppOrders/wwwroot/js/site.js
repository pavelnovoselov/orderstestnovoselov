﻿
function modalClose() {
    $("#editElement").modal("hide");
}

function showFormAddorEditOrder(el) {
    let modal = $("#editElement");
    let content = modal.find(".modal-content");
    content.html("");
    let data = {
        Id: $(el).data("id")
    }

    $.ajax({
        url: `/Home/ShowFormAddorEditOrder`,
        type: "GET",
        data: data
    }).done(function (result) {
        content.html(result);
        modal.modal('show');
    });
}

function checkOnChangeOrder(el) {
    let id = $(el).data("id");
    let data = {
        Id: id,
        NameNumber: document.getElementById('order_item_namber').value,
        Date: document.getElementById('order_item_date').value,
        ProviderId: document.getElementById('order_item_providerid').value,
    }
    $.ajax({
        url: "/Home/CheckOnChangeOrder",
        data: data,
        type: "POST",
    }).done(function (result) {
        debugger;
        if (result != "") {
            $('#mesage_namber_error').html(result);
            return;
        }
        else {
            $('#mesage_namber_error').html("");
            saveAddorEditOrder(el)
        }
    });
}

function saveAddorEditOrder(el) {
    let id = $(el).data("id");
    let data = {
        Id: id,
        NameNumber: document.getElementById('order_item_namber').value,
        Date: document.getElementById('order_item_date').value,
        ProviderId: document.getElementById('order_item_providerid').value,
    }
    $.ajax({
        url: "/Home/SaveAddorEditOrder",
        data: data,
        type: "POST",
    }).done(function (result) {
        $("#editElement").modal("hide");
        if (result.id != id) {
            $('#tbody_orders').append(`<tr class="row_order" data-id="${result.id}" onclick="showOrder(this)"><td>${result.id}</td><td>${result.nameNumber}</td><td>${result.date}</td><td>${result.providerName}</td></tr>`);

        } else {
            $(`#tbody_orders tr[data-id=${result.id}]`).replaceWith(`<tr class="row_order" data-id="${result.id}" onclick="showOrder(this)"><td>${result.id}</td><td>${result.nameNumber}</td><td>${result.date}</td><td>${result.providerName}</td></tr>`);
            $(`#tbody_order tr[data-id=${result.id}]`).replaceWith(`<tr data-id="${result.id}" onclick="showOrder(this)"><td>${result.id}</td><td>${result.nameNumber}</td><td>${result.date}</td><td>${result.providerName}</td></tr>`);
        }

    });
}

function showOrder(el) {
    let id = $(el).data("id");
    let url = "/Home/FormOrder";
    window.open(pb + url + `?id=${id}`);
}

function showOrders() {
    let url = "/Home/Index";
    window.open(pb + url);
}

function showFormDeleteOrder(el) {
    let modal = $("#editElement");
    let content = modal.find(".modal-content");
    let data = {
        Id: $(el).data("id")
    }
    content.html("");

    $.ajax({
        url: `/Home/FormDeleteOrder`,
        type: "GET",
        data: data
    }).done(function (result) {
        content.html(result);
        modal.modal('show');
    });
}

function deleteOrder(id) {
    let data = {
        Id: id
    }

    $.ajax({
        url: "/Home/DeleteOrder",
        data: data,
        async: false,
        type: "POST"
    }).done(function () {
        $("#editElement").modal("hide");
        window.close();
    });
    showOrders();
    $(`#tbody_orders tr[data-id=${id}]`).remove();
}

function showFormOrderItem(el) {
    let modal = $("#editElement");
    let content = modal.find(".modal-content");
    let data = {
        Id: $(el).data("id"),
        OrderId: $(el).data("orderid")
    }
    content.html("");

    $.ajax({
        url: `/Home/ShowFormAddorEditOrderItem`,
        type: "GET",
        data: data
    }).done(function (result) {
        content.html(result);
        modal.modal('show');
    });
}

function checkOnChangeOrderItem(el) {
    let orderId = $(el).data("orderid");
    let id = $(el).data("id");
    let data = {
        Id: id,
        OrderId: parseInt(orderId),
        Name: document.getElementById('order_item_name').value,
    }
    $.ajax({
        url: "/Home/CheckOnChangeOrderItem",
        data: data,
        type: "POST",
    }).done(function (result) {
        if (result != "") {
            $('#mesage_name_error').html(result);
            return;
        }
        else {
            $('#mesage_name_error').html("");
            saveAddorEditOrderItem(el)
        }
    });
}

function saveAddorEditOrderItem(el) {
    let orderId = $(el).data("orderid");
    let id = $(el).data("id");
    let data = {
        Id: id,
        OrderId: parseInt(orderId),
        Name: document.getElementById('order_item_name').value,
        Quantity: document.getElementById('order_item_quantity').value.replace('.',','),
        Unit: document.getElementById('order_item_unit').value
    }

    $.ajax({
        url: "/Home/SaveAddOrderItem",
        data: data,
        type: "POST",
    }).done(function (result) {
        $("#editElement").modal("hide");
        if (result.id == id) {
            $(`#tbody_orderitems tr[data-id=${result.id}]`).replaceWith(`<tr data-id="${result.id}"><td class="td_menu"><a data-id="${result.id}" onclick="showFormOrderItem(this)" class="fa fa-pencil text-info td_pencil"></a><a data-id="${result.id}" onclick="showFormDeleteOrderItem(this)"><i class="fa fa-trash text-danger cursor-pointer"></i></a></td><td>${result.name}</td><td>${result.quantity}</td><td>${result.unit}</td></tr>`);
        }
        else {
            $('#tbody_orderitems').append(`<tr data-id="${result.id}"><td class="td_menu"><a data-id="${result.id}" onclick="showFormOrderItem(this)" class="fa fa-pencil text-info td_pencil"></a><a data-id="${result.id}" onclick="showFormDeleteOrderItem(this)"><i class="fa fa-trash text-danger cursor-pointer"></i></a></td><td>${result.name}</td><td>${result.quantity}</td><td>${result.unit}</td></tr>`);
        }
    });
}

function showFormDeleteOrderItem(el) {
    let modal = $("#editElement");
    let content = modal.find(".modal-content");
    let data = {
        Id: $(el).data("id")
    }
    content.html("");

    $.ajax({
        url: `/Home/FormDeleteOrderItem`,
        type: "GET",
        data: data
    }).done(function (result) {
        content.html(result);
        modal.modal('show');
    });
}

function deleteOrderItem(id) {
    let data = {
        Id: id
    }

    $.ajax({
        url: "/Home/DeleteOrderItem",
        data: data,
        type: "POST"
    }).done(function () {
        $("#editElement").modal("hide");
    });

    $(`#tbody_orderitems tr[data-id=${id}]`).remove();
}






//function saveAddorEditOrder(el) {
//    let id = $(el).data("id");
//    let data = {
//        Id: id,
//        IdOrderItem: document.getElementById('id_order_item').value,
//        NameNumber: document.getElementById('order_item_namber').value,
//        Date: document.getElementById('order_item_date').value,
//        ProviderId: document.getElementById('order_item_providerid').value,
//        Name: document.getElementById('order_item_name').value,
//        Quantity: document.getElementById('order_item_quantity').value,
//        Unit: document.getElementById('order_item_unit').value
//    }
//    $.ajax({
//        url: "/Home/SaveAddorEditOrder",
//        data: data,
//        type: "POST",
//    }).done(function (result) {
//        debugger;
//        $("#editElement").modal("hide");
//        if (result.id == id) {
//            $(`#tbody_orders tr[data-id=${result.id}]`).replaceWith(`<tr class="row_order" data-id="${result.id}"><td>${result.namber}</td><td>${result.date}</td<td>${result.providerId}</td>><td>${result.name}</td><td>${result.Quantity}</td><td>${result.unit}</td></tr>`);
//        }
//        else {
//            $('#tbody_orders').append(`<tr class="row_order" data-id="${result.id}"><td>${result.namber}</td><td>${result.date}</td<td>${result.providerId}</td>><td>${result.name}</td><td>${result.Quantity}</td><td>${result.unit}</td></tr>`);
//        }
//    });
//}